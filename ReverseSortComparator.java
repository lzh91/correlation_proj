import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class ReverseSortComparator extends WritableComparator {

    protected ReverseSortComparator(){
	super(DoubleWritable.class, true);
    }

    public int compare(WritableComparable a, WritableComparable b){
	DoubleWritable o1 = (DoubleWritable) a;
	DoubleWritable o2 = (DoubleWritable) b;

	if (o1.get() < o2.get()){
	    return 1;
	} else if (o1.get() > o2.get()){
	    return -1;
	} else return 0;
    }
}