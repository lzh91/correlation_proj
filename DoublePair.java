import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

public class DoublePair implements Writable{

    double double1;
    double double2;

    public DoublePair(){
	double1 = 0;
	double2 = 0;
    }

    public DoublePair(double double1, double double2){
	this.double1 = double1;
	this.double2 = double2;
    }

    public double getDouble1(){
	return double1;
    }

    public double getDouble2(){
	return double2;
    }

    public void setDouble1(double val){
	double1 = val;
    }

    public void setDouble2(double val){
	double2 = val;
    }

    public void write(DataOutput out) throws IOException{
	out.writeDouble(double1);
	out.writeDouble(double2);
    }

    public void readFields(DataInput in) throws IOException{
	double1 = in.readDouble();
	double2 = in.readDouble();
    }

    public int compareTo(DoublePair o){
	if (double1 < o.double1){
	    return -1;
	} else if (double1 > o.double1) {
	    return 1;
	} else if (double2 < o.double2) {
	    return -1;
	} else if (double2 > o.double2) {
	    return 1;
	} else return 0;
    }
}