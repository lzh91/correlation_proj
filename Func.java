public abstract class Func {

    public abstract double f(double d);
}