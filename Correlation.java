import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Collections;
import java.lang.Math;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class Correlation{
    public static class Map1 
	extends Mapper<WritableComparable, Text, Text, DoublePair>{
	final static Pattern WORD_PATTERN = Pattern.compile("\\w+");

	private String targetGram = null;
	private int funcNum = 0;

	private Text word = new Text();

	@Override
	public void setup(Context context){
	    
	    targetGram = context.getConfiguration().
		get("targetWord").toLowerCase();
	    
	    try{
		funcNum = Integer.parseInt(context.getConfiguration().
					   get("funcNum"));
	    } catch (NumberFormatException e){

	    }
	}

	@Override
	    public void map(WritableComparable docID, Text docContents, 
			    Context context) throws IOException, InterruptedException{
	    Matcher matcher = WORD_PATTERN.matcher(docContents.toString());
	    Func func = funcFromNum(funcNum);
	    
	    List<Text> words = new ArrayList<Text>(); 
	    List<Integer> targetWordPos = new ArrayList<Integer>();
	    while (matcher.find()){
		String lex = matcher.group().toLowerCase();
		word = new Text();
		word.set(lex);
		words.add(word);

		if (lex.equals(targetGram))
		    targetWordPos.add(words.size() - 1);
	    }

	    for (int i = 0; i < words.size(); i++){
		Text text = words.get(i);
		DoublePair pair = new DoublePair(1.0, 0.0);
		
		if (!targetWordPos.isEmpty()){
		    if (!text.toString().equals(targetGram)){
			int minVal = Integer.MAX_VALUE;
			for (int j = 0; j < targetWordPos.size(); j++){
			    int distance = Math.abs(i - targetWordPos.get(j));
			    if (distance < minVal)
				minVal = distance;
			}
			pair.setDouble2(func.f((double)minVal));
		    }
		}

		context.write(text, pair);	
	    }
	}

	private Func funcFromNum(int funcNum){
	    Func func = null;

	    switch(funcNum){
	    case 0:
		func = new Func(){
			public double f(double d){
			    return d == Double.POSITIVE_INFINITY ? 0.0 : 1.0;
			}
		    };
		break;
	    case 1:
		func = new Func(){
			public double f(double d){
			    return d == Double.POSITIVE_INFINITY ? 0.0 : 1.0 + 1.0 / d;
			}
		    };
		break;
	    case 2:
		func = new Func(){
			public double f(double d){
			    return d == Double.POSITIVE_INFINITY ? 0.0 : 1.0 + Math.sqrt(d);
			}
		    };
		break;
	    }

	    return func;
	}
    }

    public static class Combine1 extends Reducer<Text, DoublePair, Text, DoublePair>{
	@Override
	    public void reduce(Text key, Iterable<DoublePair> values, Context context) 
	    throws IOException, InterruptedException{
	    double allOccurrence = 0.0;
	    double coOccurrence = 0.0;
	    
	    for (DoublePair pair: values){
		allOccurrence += pair.getDouble1();
		coOccurrence += pair.getDouble2();
	    }

	    context.write(key, new DoublePair(allOccurrence, coOccurrence));
	}
    }

    public static class Reduce1 extends Reducer<Text, DoublePair, DoubleWritable, Text>{
	@Override
	    public void reduce(Text key, Iterable<DoublePair> values, Context context)
	    throws IOException, InterruptedException{
	    double allOccurrence = 0.0;
	    double coOccurrence = 0.0;

	    for (DoublePair pair: values){
		allOccurrence += pair.getDouble1();
		coOccurrence += pair.getDouble2();
	    }

	    double result = 0.0;
	    if (coOccurrence > 0){
		result = coOccurrence * 
		    Math.pow(Math.log(coOccurrence), 3.0) / allOccurrence;
	    }
	    

	    context.write(new DoubleWritable(result), key);
	}
    }

    public static class Map2 extends Mapper<DoubleWritable, Text, DoubleWritable, Text>{

    }

    public static class Reduce2 extends Reducer<DoubleWritable, Text, DoubleWritable, Text>{
	
	int n = 0;
	static int N_TO_OUTPUT = 100;

	@Override
	    protected void setup(Context context){
	    n = 0;
	}

	@Override
	    public void reduce(DoubleWritable key, Iterable<Text> values, Context context)
	    throws IOException, InterruptedException{

	    for (Text value: values){
		if (n++ >= N_TO_OUTPUT) return;
		context.write(key, value);
	    }
	}
    }
    

    public static void main(String[] rawArgs) throws Exception{
	GenericOptionsParser parser = new GenericOptionsParser(rawArgs);
	Configuration conf = parser.getConfiguration();
	String[] args = parser.getRemainingArgs();

	boolean runJob2 = conf.getBoolean("runJob2", true);
	boolean combiner = conf.getBoolean("combiner", false);
	
	System.out.println("Target word: " + conf.get("targetWord"));
	System.out.println("Function Number: " + conf.get("funcNum"));

	if (runJob2)
	    System.out.println("running both jobs");
	else 
	    System.out.println("For debugging, only running job 1");

	if (combiner)
	    System.out.println("using combiner");
	else 
	    System.out.println("NOT using combiner");


	Path inputPath = new Path(args[0]);
	Path middleOut = new Path(args[1]);
	Path finalOut = new Path(args[2]);
	FileSystem hdfs = middleOut.getFileSystem(conf);
	int reduceCount = conf.getInt("reduces", 32);

	if (hdfs.exists(middleOut)){
	    System.err.println("can't run: " + middleOut.toUri().toString());
	    System.exit(1);
	}
	

	if (finalOut.getFileSystem(conf).exists(finalOut)){
	    System.err.println("can't run: " + finalOut.toUri().toString());
	    System.exit(1);
	}

	{
	    Job firstJob = new Job(conf, "job1");
	    
	    firstJob.setJarByClass(Map1.class);
	    
	    firstJob.setMapOutputKeyClass(Text.class);
	    firstJob.setMapOutputValueClass(DoublePair.class);
	    firstJob.setOutputKeyClass(DoubleWritable.class);
	    firstJob.setOutputValueClass(Text.class);
	    
	    firstJob.setMapperClass(Map1.class);
	    firstJob.setReducerClass(Reduce1.class);
	    firstJob.setNumReduceTasks(reduceCount);
	    
	    if (combiner){
		firstJob.setCombinerClass(Combine1.class);
	    }

	    firstJob.setInputFormatClass(SequenceFileInputFormat.class);
	    if (runJob2)
		firstJob.setOutputFormatClass(SequenceFileOutputFormat.class);
	    
	    FileInputFormat.addInputPath(firstJob, inputPath);
	    FileOutputFormat.setOutputPath(firstJob, middleOut);

	    firstJob.waitForCompletion(true);	    
	}

	if (runJob2){
	    Job secondJob = new Job(conf, "job2");

	    secondJob.setJarByClass(Map1.class);
	    secondJob.setMapOutputKeyClass(DoubleWritable.class);
	    secondJob.setMapOutputValueClass(Text.class);
	    secondJob.setOutputKeyClass(DoubleWritable.class);
	    secondJob.setOutputValueClass(Text.class);
	    secondJob.setSortComparatorClass(ReverseSortComparator.class);

	    secondJob.setMapperClass(Map2.class);
	    secondJob.setReducerClass(Reduce2.class);

	    secondJob.setInputFormatClass(SequenceFileInputFormat.class);
	    secondJob.setOutputFormatClass(TextOutputFormat.class);
	    secondJob.setNumReduceTasks(1);
	    
	    FileInputFormat.addInputPath(secondJob, middleOut);
	    FileOutputFormat.setOutputPath(secondJob, finalOut);
	    
	    secondJob.waitForCompletion(true);
	}
    }
}
