
SOURCES = $(wildcard *.java)
TARGET = correlation.jar
CLASSPATH = /home/hduser/hadoop/hadoop-2.2.0/share/hadoop/common/hadoop-common-2.2.0.jar:/home/hduser/hadoop/hadoop-2.2.0/share/hadoop/mapreduce/hadoop-mapreduce-client-core-2.2.0.jar:/home/hduser/hadoop/hadoop-2.2.0/share/hadoop/common/lib/commons-cli-1.2.jar:/home/hduser/hadoop/hadoop-2.2.0/share/hadoop/common/lib/hadoop-annotations-2.2.0.jar

COMPAT_FLAGS = -source 6 -target 6

JAVAC = javac -g $(COMPAT_FLAGS) -deprecation -cp $(CLASSPATH)
JAR = jar

all: ${TARGET}

${TARGET}: classes ${SOURCES}
	${JAVAC} -d classes ${SOURCES}
	${JAR} cf ${TARGET} -C classes .

classes:
	mkdir classes

clean:
	rm -rf classes $(TARGET)

.PHONY: clean all
